# NeverTheLess - Helm Repository

##### Table of contents

- [Get Started](#get-started)
  - [Initialization](#initialization)
  - [Delete](#delete)
- [Charts](#charts)
  - [azuredevops-agent](#azuredevops-agent)
  - [azuredevops-deploymentgroup](#azuredevops-deploymentgroup)

## Get Started

### Initialization
To use your new Chart repository, run on your local computer:
```bash
# Add Helm Repository - trought snippet
curl https://gitlab.com/nevertheless.space/helm-charts/-/snippets/2249091/raw/main/add-ntl-helm-repo.sh | sh

# Add Helm Repository - Expanded
helm repo add nevertheless.space https://gitlab.com/api/v4/projects/24399618/packages/helm/stable
helm repo update

# Install Helm Chart
helm install --namespace=<namespace-name> --create-namespace <release-name> nevertheless.space/<chart-name>:<chart-version>
```

### Delete

To uninstall the Helm application:

```bash
helm uninstall <release-name> --namespace <namespace>
kubectl delete namespace <namespace>
```

## Charts

### azuredevops-agent

- Current version documentation: [README.md](https://gitlab.com/nevertheless.space/helm-charts/-/blob/master/charts/azuredevops-agent/README.md).
- Description: Azure DevOps Agent for Kubernetes.

### azuredevops-deploymentgroup

- Current version documentation: [README.md](https://gitlab.com/nevertheless.space/helm-charts/-/blob/master/charts/azuredevops-deploymentgroup/README.md).
- Description: Azure DevOps Deployment Group Agent for Kubernetes.
